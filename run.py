#!/usr/bin/env python
from aiohttp import web
from aiohttp_session.redis_storage import RedisStorage

from middlewares import middleware, db
import settings
from aioredis import create_redis_pool
# from aiopg.sa import create_engine
# from aiohttp_security import setup as setup_security
from aiohttp_security import SessionIdentityPolicy
from aiohttp_security import setup as setup_security
from aiohttp_session import setup as setup_session
from auth_policy import DBAuthorizationPolicy
from router import ROUTES, setup_routes
import logging
_logger = logging.getLogger(__name__)


async def create():
    await db.set_bind(settings.DATABASE_CONNECTION_STRING)
    await db.gino.create_all()
    await db.pop_bind().close()


async def setup_redis(app):

    pool = await create_redis_pool(
        settings.REDDIS_PARAMS
    )

    async def close_redis(app):
        pool.close()
        await pool.wait_closed()

    app.on_cleanup.append(close_redis)
    app['redis_pool'] = pool
    return pool


async def init_app():
    app = web.Application(middlewares=middleware)
    app["config"] = {"gino":settings.DB_ARGS}
    setup_routes(app, ROUTES)

    await create()
    db.init_app(app)

    redis_pool = await setup_redis(app)

    setup_session(app, RedisStorage(redis_pool))
    setup_security(
        app,
        SessionIdentityPolicy(),
        DBAuthorizationPolicy(db)
    )

    return app


def main():
    app = init_app()
    web.run_app(app)


if __name__ == '__main__':
    main()
