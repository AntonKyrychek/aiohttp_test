from middlewares import db
from datetime import datetime
from sqlalchemy import func


class LocalDbModel(db.Model):
    __abstract__ = True

    def __str__(self):
        obj_id = hex(id(self))
        columns = self.__table__.c.keys()

        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in columns)
        return '<%s %s(%s)>' % (obj_id, self.__class__.__name__, values)

    @classmethod
    async def get(cls, id):
        return await db.first(cls.query.where(cls.id == id))

    @classmethod
    def _premap_objects(cls, q, joined_object):
        if isinstance(joined_object, list):
            params = {joined_object[0].__name__.lower(): joined_object[0]}
            loaders = cls.load(**params)
            del joined_object[0]
            for j in joined_object:
                params = {j.__name__.lower(): j}
                loaders = loaders.load(**params)
        else:
            params = {joined_object.__name__.lower(): joined_object}
            loaders = cls.load(**params)

        return q.gino.load(loaders)

    @classmethod
    async def get_list(cls, filtered_by=None, ordered_by=None, limit=None, offset=None, joined_object=None):
        """
        Method for getting list of objects in a simplier way then in Gino 
        
        :param filtered_by: cls.Column == Value or list of these
        :param ordered_by: has to be with desc() or asc()
        :param limit: int()
        :param offset: int()
        :param joined_object: LocalDbModel

        :return: list of objects
        """

        if joined_object:
            q = cls
            if isinstance(joined_object, list):
                for j in joined_object:
                    q = q.join(j)
            else:
                q = q.join(joined_object)

            q = q.select()
        else:
            q = cls.query

        if filtered_by is not None:
            if isinstance(filtered_by, list):
                for filter in filtered_by:
                    q = q.where(filter)
            else:
                q = q.where(filtered_by)

        if ordered_by is not None:
            q = q.order_by(ordered_by)
        if limit is not None:
            q = q.limit(limit)
        if offset is not None:
            q = q.offset(offset)

        if joined_object:
            objects = await cls._premap_objects(q, joined_object).all()
        else:
            objects = await q.gino.all()

        return objects

    def as_dict(self):
        def fu(kls, x):
            value = getattr(kls, x)
            if isinstance(value, datetime):
                value = int(value.timestamp())
            elif isinstance(value, LocalDbModel):
                value = value.as_dict()
            return value

        keys = self.__table__.c.keys()
        model_names = [*self.__dict__.keys()]
        model_names.remove('__values__')
        model_names.remove('__profile__')
        keys.extend(model_names)

        return {x: fu(self, x) for x in keys}


class CreatedUpdatedMixin:
    __abstract__ = True

    created_on = db.Column(db.TIMESTAMP, default=db.func.now())
    updated_on = db.Column(db.TIMESTAMP, default=db.func.now(), onupdate=db.func.now())


class StreakPostsMixin:
    __abstract__ = True

    record_streak_days = db.Column(db.Integer(), default=0)
    record_streak_hours = db.Column(db.Integer(), default=0)

    current_streak_days = db.Column(db.Integer(), default=0)
    current_streak_hours = db.Column(db.Integer(), default=0)


class User(LocalDbModel, StreakPostsMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer(), primary_key=True)
    username = db.Column(db.String(), unique=True)
    email = db.Column(db.String(), nullable=True)
    hashed_password = db.Column(db.String(), nullable=False)

    def as_dict(self):
        d = super().as_dict()
        d.pop("hashed_password")
        d.pop("id")
        return d

    @classmethod
    async def get_user(cls, username):
        return await db.first(cls.query.where(
            cls.username == username))


class Post(LocalDbModel, CreatedUpdatedMixin):
    __tablename__ = 'posts'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())
    content = db.Column(db.String())
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    tags = db.Column(db.String(), nullable=True)

    def as_dict(self):
        d = super().as_dict()
        # effing loaders
        d["tags"] = d["tags"].split(" ") if d.get("tags") else None
        d.pop("user_id")
        if d.get("user"):
            d["user"] = d["user"]["username"]
        return d

    @classmethod
    async def get_posts(cls, filtered_by=None, ordered_by=None, limit=10, offset=None):
        return await cls.get_list(filtered_by, ordered_by, limit, offset, joined_object=User)

    @classmethod
    async def get_latests_post_dt(cls, user_id):
        q = db.select([func.max(cls.created_on)]).where(cls.user_id == user_id)
        dt = await q.gino.first()
        return dt.values()[0]
