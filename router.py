import logging

from views import create_post, get_posts, login, register_user, logout, my_posts, get_user,get_rating
_logger = logging.getLogger(__name__)



ROUTES = [
    {"route": "/posts",              "action": "POST",    "handler": create_post,   "name": "create_post"},
    {"route": "/user",               "action": "GET",     "handler": get_user,      "name": "get_user"},
    {"route": "/rating",             "action": "GET",     "handler": get_rating,    "name": "get_rating"},
    {"route": "/posts",              "action": "GET",     "handler": get_posts,     "name": "get_posts"},
    {"route": "/my_posts",           "action": "GET",     "handler": my_posts,      "name": "my_posts"},
    {"route": "/login",              "action": "POST",    "handler": login,         "name": "login"},
    {"route": "/logout",              "action": "GET",    "handler": logout,        "name": "logout"},
    {"route": "/register",           "action": "POST",    "handler": register_user, "name": "register"},
]


def setup_routes(app, handlers):
    for handler in handlers:
        if handler.get("action", "").upper() == "POST":
            route = app.router.add_post(handler["route"], handler["handler"], name=handler.get("name", None))
            cors = handler.get("cors")
            if cors:
                app.cors.add(route, cors)
        elif handler.get("action", "").upper() == "GET":
            app.router.add_get(handler["route"], handler["handler"], name=handler.get("name", None))
        elif handler.get("action", "").upper() == "STATIC":
            try:
                route = handler.get('route', '/static/')
                app.router.add_static(route, path=handler["path"], name=handler.get("name", None))
            except ValueError as err:
                _logger.debug("Static directory not defined. SKIPPED")
        else:
            pass
