import bcrypt


def generate_password_hash(password, salt=None):
    salt = salt if salt else bcrypt.gensalt()
    password_bin = password.encode('utf-8')
    return bcrypt.hashpw(password_bin, salt).decode('utf-8')


def check_password_hash(plain_password, password_hash):
    plain_password_bin = plain_password.encode('utf-8')
    password_hash_bin = password_hash.encode('utf-8')
    return bcrypt.checkpw(plain_password_bin, password_hash_bin)
