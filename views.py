from asyncpg.exceptions import UniqueViolationError
from aiohttp import web
from logging import getLogger
from aiohttp_security import remember, forget, authorized_userid

from core import check_get_user, add_post_records
from redis_utils import add_to_redis, check_fobidden
from password_utils import generate_password_hash
from models import Post, User

_logger = getLogger(__name__)


async def create_post(request: web.Request):
    user = await check_get_user(request)
    body = dict(await request.post())

    body["user_id"] = user.id
    post = Post(**body)
    post = await post.create()
    await add_post_records(user, post)
    return web.json_response({"status": "OK"})


async def _user_posts(request, user: User = None):

    limit = int(request.query.get('limit', 10))
    offset = (int(request.query.get('page', 1)) - 1) * limit
    params = {
        "limit": limit,
        "offset": offset,
    }

    filter_by = []
    if user:
        filter_by.append(Post.user_id == user.id)

    tags = request.query.get("tags")
    if tags:
        filter_by.extend([Post.tags.like(f"%{tag}%") for tag in tags.split(" ")])

    if filter_by:
        params["filtered_by"] = filter_by

    posts = await Post.get_posts(**params)
    posts = [post.as_dict() for post in posts]
    return web.json_response(posts)


async def my_posts(request: web.Request):
    user = await check_get_user(request)
    return await _user_posts(request, user)


async def get_posts(request: web.Request):
    params = [request]
    await check_get_user(request)
    username = request.query.get('username')
    author = await User.get_user(username)
    params.append(author)
    return await _user_posts(*params)


async def get_user(request: web.Request):
    user = await check_get_user(request)
    return web.json_response(user.as_dict())


async def get_rating(request: web.Request):
    allowed_intervals = {"24h": User.record_streak_days.desc(), "1h": User.record_streak_hours.desc()}

    limit = int(request.query.get('limit', 10))
    offset = (int(request.query.get('page', 1)) - 1) * limit

    interval = request.query["interval"]
    if interval not in allowed_intervals:
        raise web.HTTPBadRequest
    ordered_by = allowed_intervals[interval]
    users = await User.get_list(ordered_by=ordered_by, limit=limit, offset=offset,)
    users = [user.as_dict() for user in users]

    return web.json_response(users)


async def login(request: web.Request):
    salt_redis = b'$2b$12$4pgJunNf2qmDH5RegSBWju'

    user = await authorized_userid(request)
    if user:
        return web.json_response({"name": user.username})

    unique = generate_password_hash(request.headers["User-Agent"], salt_redis)

    await check_fobidden(request.app, unique)
    await add_to_redis(request.app, unique)

    body = await request.post()

    user = await User.get_user(body["username"])
    if not user:
        raise web.HTTPUnauthorized
    from password_utils import check_password_hash
    if check_password_hash(body["password"], user.hashed_password):
        response = web.json_response({"user": user.username})
        await remember(request, response, user.username)
    else:
        raise web.HTTPUnauthorized

    return response


async def register_user(request: web.Request):
    body = await request.post()

    user = User(**{
        "hashed_password": generate_password_hash(body["password"]),
        "username": body["username"],
    })

    try:
        await user.create()
    except UniqueViolationError:
        return web.json_response({"error": "Username exists"}, status=500)
    response = web.json_response(user.as_dict())
    await remember(request, response, user.username)
    return response


async def logout(request):
    await check_get_user(request)
    response = web.json_response({"data": "done"})
    await forget(request, response)
    return response
