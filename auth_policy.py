from aiohttp_security.abc import AbstractAuthorizationPolicy
from models import User


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, db):
        self.db = db

    async def authorized_userid(self, identity):
        user = await User.get_user(identity)
        if user:
            return user

        return None

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False
        return True
