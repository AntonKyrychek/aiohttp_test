import os


DB_ARGS = dict(
    host=os.getenv("DB_HOST", "localhost"),
    port=os.getenv("DB_PORT", 5432),
    user=os.getenv("DB_USER", "user"),
    password=os.getenv("DB_PASS", "pas123123123"),
    database=os.getenv("DB_NAME", "db"),
)

DATABASE_CONNECTION_STRING = "postgresql://{user}:{password}@{host}:{port}/{database}".format(
    **DB_ARGS
)

REDDIS_HOST = "localhost"
REDDIS_PORT = 6379
REDDIS_PARAMS = (REDDIS_HOST, REDDIS_PORT)
