from datetime import datetime
from random import randint

from aiohttp.web_exceptions import HTTPForbidden

PREFIX_FORBIDDEN = "login_forbidden:"
PREFIX_LOGING = "cache_login:"
TTL_LOGING_FAILED = 60
TTL_LOGING_FORBIDDEN = 300


def add_random_tail(key):
    key += ":" + str(randint(0, 1000))
    return key


async def add_to_redis(app, unique):
    redis = app["redis_pool"]
    value = str(int(datetime.now().timestamp()))
    key = PREFIX_LOGING + unique
    conn, values = await redis.scan(match=key + ":*")
    while conn:
        conn, values_ext = await redis.scan(cursor=conn, match=key + ":*")
        values.extend(values_ext)

    if len(values) >= 5:
        await add_fobidden(app, unique)
        raise HTTPForbidden()

    key = add_random_tail(key)
    await redis.set(key, value, expire=TTL_LOGING_FAILED)


async def check_fobidden(app, unique):
    key = PREFIX_FORBIDDEN + ":" + unique
    if await app["redis_pool"].get(key):
        raise HTTPForbidden()


async def add_fobidden(app, unique):
    value = str(int(datetime.now().timestamp()))
    key = PREFIX_FORBIDDEN + ":" + unique
    await app["redis_pool"].set(key, value, expire=TTL_LOGING_FORBIDDEN)
