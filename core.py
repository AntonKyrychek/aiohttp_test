from aiohttp import web
from aiohttp_security import authorized_userid
from dateutil.relativedelta import relativedelta

from models import User, Post


async def check_get_user(request: web.Request):
    user = await authorized_userid(request)
    if not user:
        raise web.HTTPUnauthorized
    return user


def check_streak(current_streak, record_streak, interval, last_post_d, current_post_d):
    interval_date = last_post_d + interval
    if interval_date == current_post_d:
        current_streak += 1
        if current_streak > record_streak:
            record_streak = current_streak
    elif interval_date < current_post_d:
        current_streak = 1
    return current_streak, record_streak


async def add_post_records(user: User, current_post):
    day = relativedelta(day=1)
    hour = relativedelta(hour=1)

    last_post_dt = await Post.get_latests_post_dt(user.id)
    last_post_d = last_post_dt.date()

    cp_dt = current_post.created_on
    cp_d = cp_dt.date()
    day_streak, day_streak_record = check_streak(
        user.current_streak_days,
        user.record_streak_days,
        day,
        last_post_d,
        cp_d
    )

    last_post_h = last_post_dt.replace(minute=0, second=0, microsecond=0)
    cp_h = cp_dt.replace(minute=0, second=0, microsecond=0)

    hour_streak, hour_streak_record = check_streak(
        user.current_streak_hours,
        user.record_streak_hours,
        hour,
        last_post_h,
        cp_h)

    await user.update(
        current_streak_days=day_streak,
        record_streak_days=day_streak_record,
        current_streak_hours=hour_streak,
        record_streak_hours=hour_streak_record,
    ).apply()
